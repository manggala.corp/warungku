package com.example.warungku2.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.warungku2.Config;
import com.example.warungku2.R;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    Context context;
    private ArrayList<String>price,name,desc,menu_id;
    private String url = Config.Api;

    String token;

    public  MenuAdapter(Context context,ArrayList<String> name){
        Log.d("dancuuuuuu",name.toString());

        this.context = context;
        this.name = name;
//        this.price = price;
//        this.desc = desc;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_menu,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolder holder, int position) {

        holder.name.setText(name.get(position));
//        holder.price.setText(price.get(position));
    }

    @Override
    public int getItemCount() {

        return name.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView price,name,desc,menu_id;
        ImageView delete_type_sepatu,edit_type_sepatu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.menuName);
//            name = itemView.findViewById(R.id.menuName);
//            desc = itemView.findViewById(R.id.menuDesc);
        }
    }

}