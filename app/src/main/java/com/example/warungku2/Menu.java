package com.example.warungku2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.warungku2.adapter.MenuAdapter;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


public class Menu extends AppCompatActivity {

    ExtendedFloatingActionButton btnMenu;
    public String api =  Config.Api;
    ArrayList<String> menuList,name,price,keterangan;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    ProgressDialog progressDialog;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    public JsonArrayRequest jsonArrayRequest;
    public static final String TAG = AppCompatActivity.class.getSimpleName();
    MenuAdapter menuAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().setTitle("Daftar Menu");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        btnMenu =findViewById(R.id.addMenu);
        menuList = new ArrayList<>();
        price = new ArrayList<>();
        keterangan = new ArrayList<>();
        name = new ArrayList<>();
        recyclerView =findViewById(R.id.list_menu);

        LinearLayoutManager lm = new LinearLayoutManager(getApplicationContext());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lm);

        swipeRefreshLayout = findViewById(R.id.id_swipe);
        getData();
    }

    public void dialodLoading(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Menggambil Data ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }



    public void getData(){
        name.clear();
        price.clear();
        keterangan.clear();


        dialodLoading();


        stringRequest = new StringRequest(Request.Method.GET, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONArray hotel_arr = obj.getJSONArray("hotel");
                    for (int i = 0; i < hotel_arr.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject hotel_arrJSONObject = hotel_arr.getJSONObject(i);
                        name.add(hotel_arrJSONObject.getString("nama"));
                        keterangan.add(hotel_arrJSONObject.getString("alamat"));
                    }

                    menuAdapter = new MenuAdapter(Menu.this,name);
                    recyclerView.setAdapter(menuAdapter);
                    progressDialog.dismiss();
                    Log.i(TAG,"ketarangan cuk :" + keterangan);



                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG,"Error :" + error.toString());
            }
        });
        Log.i(TAG,"dancuuuu asem :" + stringRequest.toString());

        requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG,"Message cuk :" + requestCode);


        if(requestCode == 1){
            if (resultCode ==RESULT_OK){
                getData();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }




}